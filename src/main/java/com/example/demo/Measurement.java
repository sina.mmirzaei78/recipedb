package com.example.demo;

public enum Measurement {
    KG,
    TBSP,
    TSP,
    G,
    HG,
    ML,
    CL,
    DL
}
