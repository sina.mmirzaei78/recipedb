package com.example.demo.repository;

import com.example.demo.RecipeInstruction;
import org.springframework.data.repository.CrudRepository;

public interface RecipeInstructionRepository extends CrudRepository<RecipeInstruction,String> {
}
