package com.example.demo.repository;

import com.example.demo.Ingredient;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface IngredientRepository extends CrudRepository<Ingredient , Integer> {
    Optional<Ingredient> findByName (String name);
    Optional<Ingredient> findByNameContaining(String string);
}
