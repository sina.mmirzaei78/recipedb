package com.example.demo.repository;

import com.example.demo.Recipe;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Set;

public interface RecipeRepository extends CrudRepository<Recipe , Integer> {
    List<Recipe> findAllByNameContains(String string);

    @Query(value = "SELECT recipe.id,recipe.name,recipe.instruction_id FROM recipe JOIN recipe_ingredient JOIN ingredient ON recipe.id=recipe_ingredient.recipe_id AND recipe_ingredient.ingredient_id=ingredient.id WHERE ingredient.name=?1",nativeQuery = true)
    List<Recipe> findAllByIngredientName(String name);

    @Query(value = "SELECT recipe.id,recipe.name,recipe.instruction_id FROM recipe JOIN recipe_recipe_category JOIN recipe_category ON recipe.id=recipe_recipe_category.recipe_id AND recipe_recipe_category.recipe_category_id=recipe_category.id WHERE recipe_category.name=?1",nativeQuery = true)
    List<Recipe> findAllByCategoryName(String name);

    @Query(value = "SELECT recipe.id,recipe.name,recipe.instruction_id FROM recipe JOIN recipe_recipe_category JOIN recipe_category ON recipe.id=recipe_recipe_category.recipe_id AND recipe_recipe_category.recipe_category_id=recipe_category.id WHERE recipe_category.name IN  ?1",nativeQuery = true)
    List<Recipe> findAllByDifferentCategory(Set<String> categories);
}
