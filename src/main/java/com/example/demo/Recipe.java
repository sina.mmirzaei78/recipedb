package com.example.demo;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "recipe")
public class Recipe {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String name;

    @OneToMany(mappedBy = "recipe", cascade = CascadeType.ALL)
    @JsonIgnore
    private List<RecipeRecipeCategory> recipe_recipeCategories;

    @OneToMany(mappedBy = "recipe", cascade = CascadeType.ALL)
    @JsonIgnore
    private List<RecipeIngredient> recipeIngredients;

    @OneToOne
    @JoinColumn(name = "instructionId")
    private RecipeInstruction recipeInstruction;

    public Recipe(String name, List<RecipeRecipeCategory> recipe_recipeCategories, List<RecipeIngredient> recipeIngredients, RecipeInstruction recipeInstruction) {
        this.name = name;
        this.recipe_recipeCategories = recipe_recipeCategories;
        this.recipeIngredients = recipeIngredients;
        this.recipeInstruction = recipeInstruction;
    }

    public Recipe() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<RecipeRecipeCategory> getRecipe_recipeCategories() {
        return recipe_recipeCategories;
    }

    public void setRecipe_recipeCategories(List<RecipeRecipeCategory> recipe_recipeCategories) {
        this.recipe_recipeCategories = recipe_recipeCategories;
    }

    public List<RecipeIngredient> getRecipeIngredients() {
        return recipeIngredients;
    }

    public void setRecipeIngredients(List<RecipeIngredient> recipeIngredients) {
        this.recipeIngredients = recipeIngredients;
    }

    public RecipeInstruction getRecipeInstruction() {
        return recipeInstruction;
    }

    public void setRecipeInstruction(RecipeInstruction recipeInstruction) {
        this.recipeInstruction = recipeInstruction;
    }
}
