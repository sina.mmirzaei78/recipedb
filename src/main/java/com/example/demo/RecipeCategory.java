package com.example.demo;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "recipe_category")
public class RecipeCategory {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String name;

    @OneToMany(mappedBy = "recipeCategory", cascade = CascadeType.ALL)
    @JsonIgnore
    private List<RecipeRecipeCategory> recipe_recipeCategories;

    public RecipeCategory(String categoryName, List<RecipeRecipeCategory> recipe_recipeCategories) {
        this.name = categoryName;
        this.recipe_recipeCategories = recipe_recipeCategories;
    }

    public RecipeCategory() {
    }

    public int getCategoryId() {
        return id;
    }

    public void setCategoryId(int categoryId) {
        this.id = categoryId;
    }

    public String getCategoryName() {
        return name;
    }

    public void setCategoryName(String categoryName) {
        this.name = categoryName;
    }

    public List<RecipeRecipeCategory> getRecipe_recipeCategories() {
        return recipe_recipeCategories;
    }

    public void setRecipe_recipeCategories(List<RecipeRecipeCategory> recipe_recipeCategories) {
        this.recipe_recipeCategories = recipe_recipeCategories;
    }
}
