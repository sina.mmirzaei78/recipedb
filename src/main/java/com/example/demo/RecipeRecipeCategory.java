package com.example.demo;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

@Entity
@Table(name = "recipe_recipe_category")
public class RecipeRecipeCategory {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @ManyToOne
    @JoinColumn(name = "recipeId")
    @JsonIgnore
    private Recipe recipe;

    @ManyToOne
    @JoinColumn(name = "recipeCategoryId")
    @JsonIgnore
    private RecipeCategory recipeCategory;

    public RecipeRecipeCategory(Recipe recipe, RecipeCategory recipeCategory) {
        this.recipe = recipe;
        this.recipeCategory = recipeCategory;
    }

    public RecipeRecipeCategory() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Recipe getRecipe() {
        return recipe;
    }

    public void setRecipe(Recipe recipe) {
        this.recipe = recipe;
    }

    public RecipeCategory getRecipeCategory() {
        return recipeCategory;
    }

    public void setRecipeCategory(RecipeCategory recipeCategory) {
        this.recipeCategory = recipeCategory;
    }
}
